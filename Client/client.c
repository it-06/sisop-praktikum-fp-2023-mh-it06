#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#define PORT 4443

int main(int argc, char const *argv[])
{
    char sendConnection[2048] = {0};
    char acceptConnection[2048] = {0};
    if (!getuid() == 0)
    {
        if (argc != 5)
        {
            printf("<USAGE>\n./[program_client_database] -u [username] -p [password]\n");
            return 1;
        }
        if ((strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0))
        {
            printf("<USAGE>\n./[program_client_database] -u [username] -p [password]\n");
            return 1;
        }
    }
    int test;
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    int test2;
    char *hello = "halo aku client";
    char buffer[2048] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        test=1;
        test2=1;
        printf("\n ga bisa buat socket  \n");
        return -1;
    }
    memset(&serv_addr, '0', sizeof(serv_addr));
    test2=1;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    test2=1;
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
         test=1;
         test2=0;
        printf("\nsalah alamat  \n");
        return -1;
    }

    /* code */

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
         test=1;
         test2=0;
        printf("\nga nyambung  \n");
        return -1;
    }
    int verifikasi = 0;

    if (getuid() == 0)
    {
         test=1;
         test2=1;
        sprintf(sendConnection, "root\troot");
    }else{
         test=0;
         test2=1;
        sprintf(sendConnection, "%s\t%s", argv[2], argv[4]);
    }

    send(sock, sendConnection, strlen(sendConnection), 0);
    read(sock, acceptConnection, 2048);
    printf("%s", acceptConnection);
    if (strcmp(acceptConnection, "bisa login \n") == 0)
    {
         test=1;
         test2=0;
        verifikasi = 1;
    }
    else if (strcmp(acceptConnection, "gabisa login \n") == 0)
    {
         test=0;
         test2=1;
        return 0;
    }
     test=1;
    memset(sendConnection, 0, 2048);
    memset(acceptConnection, 0, 2048);
     test2=0;
    while (verifikasi)
    {

        printf(">> ");
        gets(sendConnection);
        if (strcmp(sendConnection,"") ==0)
        {
            test2=1;
            strcpy(sendConnection,"dummy");
        }

        send(sock, sendConnection, strlen(sendConnection), 0);
        read(sock, acceptConnection, 2048);
        printf("%s", acceptConnection);

        if (strcmp(acceptConnection, " keluar\n") == 0)
        {
             test=0;
            return 0;
        }
        memset(sendConnection, 0, 2048);
        memset(acceptConnection, 0, 2048);
    }

    return 0;
}
