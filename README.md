# sisop-praktikum-FP-2023-MH-IT06

# Anggota

| Nama                   | NRP  |
|------------------------|------------------|
| Mutiara Nurhaliza     | 5027221010       |

## Daftar Isi
- [Deskripsi](#deskripsi-soal-1)
- [Penyelesaian](#penyelesaian-soal-1)
- [Output](#Output-soal-1)
- [Revisi](#Output-soal-1)
- [Kendala](#Output-soal-1)

## Deskripsi Soal

#### Sistem Database Sederhana
- Program server berjalan sebagai daemon.
- Untuk bisa akses console database, perlu buka program client (kalau di Linux seperti command mysql di bash).
- Program client dan utama berinteraksi lewat socket.
- Program client bisa mengakses server dari mana saja.

## Penyelesaian Soal
### A. Autentikasi
- Terdapat user dengan username dan password  yang digunakan untuk mengakses database yang merupakan haknya (database yang memiliki akses dengan username tersebut). Namun, jika user merupakan root (sudo), maka bisa mengakses semua database yang ada.
Catatan: Untuk hak tiap user tidak perlu didefinisikan secara rinci, cukup apakah bisa akses atau tidak.
- Username, password, dan hak akses database disimpan di suatu database juga, tapi tidak ada user yang bisa akses database tersebut kecuali mengakses menggunakan root.
- User root sudah ada dari awal.

```
//mengecek apakah path atau file yang dicari ada
int cekDataTersedia(char *data, char *path, int column)
{
    memset(data_exist, 0, sizeof(data_exist)); 
    int check = 0; // sebagai penanda keberadaan data
    FILE *fptr;
    fptr = fopen(path, "r+"); // Membuka file dalam mode read/write
    if (fptr == NULL)//error handling
    {
        strcpy(pesan, "File Tidak Dapat Dibuka\n"); 
    }
    char line[500] = {0}; // menyimpan satu baris teks dari file
    char datatemp[2048] = {0}; // enyimpan data sementara yang diperiksa dari baris file

    int i = 0; // menghitung nomor baris di file
    int j = 0; // menghitung indeks data yang ditemukan

    while (fgets(line, sizeof(line), fptr)) // Loop untuk membaca file baris per baris
    {
        if (column != 1) //pengecekkan
        {
            char tmp[2048] = {0};//simpan salinan baris
            strcpy(tmp, line);//Menyalin isi dari line ke dalam tmp
            char *token = strtok(tmp, "\t");//membagi tmp menjadi token-token terpisah berdasarkan delimiter "\t" (tab).
            for (int i = 0; i < column; i++) // Mencari data di dalam kolom
            {
                token = strtok(NULL, "\t");
            }
            strcpy(datatemp, token); // Menyalin data dari kolom yang ditentukan ke dalam variabel datatemp
        }
        else
        {
            strcpy(datatemp, line); // jika column != 1 maka baris keseluruhan akan disalin ke datatemp
        }

        if (strcmp(datatemp, data) == 0) // Membandingkan data yang ada dengan data yang ingin ditemukan
        {
            check = 1; //menunjukkan bahwa data ada
            data_exist[j] = i - 1; // Menyimpan nomor baris (indeks) data yang ditemukan dalam array data_exist
            j++; // Menaikkan indeks untuk array data_exist
        }
        i++; // Menaikkan nomor baris file
    }
    count_data_exist = j; 
    fclose(fptr); 
    return check; 
}

// memeriksa keberadaan pasangan username dan password dalam file "users.table"
int checkUser(char *username, char *password)
{
    FILE *fptr;
    fptr = fopen("databases/init/users.table", "r+"); // Membuka file "users.table" dalam mode read/write
    if (fptr == NULL)//error handling
    {
        strcpy(pesan, "File Tidak Dapat Dibuka\n"); 
        return 0; 
    }
    char line[500] = {0}; //  menyimpan satu baris teks dari file
    char kredensial[2048] = {0}; // menyimpan data username dan password yang akan diperiksa
    sprintf(kredensial, "%s\t%s\n", username, password); //  string yang  kombinasi username dan password yang  diperiksa

    while (fgets(line, sizeof(line) - 1, fptr)) // Loop untuk membaca file baris per baris
    {
        if (strcmp(kredensial, line) == 0) // Membandingkan 
        {
            strcpy(currentUser, username); // Menyalin username ke variabel currentUser (variabel global)
            fclose(fptr); 
            return 1; 
        }
    }
    fclose(fptr); 
    return 0; 
}

```

##### Fungsi `cekDataTersedia`:
Fungsi ini bertanggung jawab untuk memeriksa keberadaan suatu data dalam suatu file yang diidentifikasi melalui path yang diberikan. Berikut adalah penjelasan rinci dari fungsi ini:

1. **Parameter:**
   - `char *data`: Data yang ingin diperiksa keberadaannya dalam file.
   - `char *path`: Path ke file yang akan diperiksa.
   - `int column`: Nomor kolom (jika lebih dari satu kolom) tempat data akan dicari.

2. **Variabel-variabel:**
   - `data_exist`: Sebuah array yang menyimpan nomor baris data yang ditemukan.
   - `pesan`: Variabel yang digunakan untuk menyimpan pesan kesalahan.
   - `check`: Sebuah penanda keberadaan data.
   - `FILE *fptr`: Pointer file untuk membuka dan membaca file.
   - `line[500]`: Array yang menyimpan satu baris teks dari file.
   - `datatemp[2048]`: Variabel untuk menyimpan data sementara dari baris file.

3. **Alur Fungsi:**
   - Fungsi ini membuka file yang ditentukan oleh `path` dalam mode read/write.
   - Menggunakan loop `while` untuk membaca file baris per baris dengan `fgets`.
   - Jika `column` bukan 1, maka fungsi membagi baris menjadi token-token terpisah berdasarkan tab (`\t`) untuk mencari data di dalam kolom yang ditentukan.
   - Membandingkan data yang ada dengan data yang ingin ditemukan menggunakan `strcmp`.
   - Jika data ditemukan, variabel `check` diatur sebagai 1 dan nomor baris data yang ditemukan disimpan dalam array `data_exist`.

4. **Output:**
   - Mengembalikan nilai `check` yang menunjukkan keberadaan atau ketiadaan data.
   - Nomor baris data yang ditemukan juga tersimpan dalam array `data_exist`.

##### Fungsi `checkUser`:
Fungsi ini bertanggung jawab untuk memeriksa keberadaan pasangan username dan password dalam file "users.table". Berikut adalah penjelasan rinci dari fungsi ini:

1. **Parameter:**
   - `char *username`: Username yang akan diperiksa.
   - `char *password`: Password yang akan diperiksa.

2. **Variabel-variabel:**
   - `pesan`: Variabel yang digunakan untuk menyimpan pesan kesalahan.
   - `currentUser`: Variabel global yang menyimpan username saat ini.
   - `FILE *fptr`: Pointer file untuk membuka dan membaca file.
   - `line[500]`: Array yang menyimpan satu baris teks dari file.
   - `kredensial[2048]`: Variabel untuk menyimpan kombinasi username dan password yang akan diperiksa.

3. **Alur Fungsi:**
   - Fungsi ini membuka file "users.table" dalam mode read/write.
   - Menggunakan loop `while` untuk membaca file baris per baris dengan `fgets`.
   - Membandingkan setiap baris dengan string yang berisi kombinasi username dan password.
   - Jika pasangan username dan password ditemukan, maka nilai 1 dikembalikan dan username disalin ke dalam variabel global `currentUser`.

4. **Output:**
   - Mengembalikan nilai 1 jika pasangan username dan password ditemukan, dan 0 jika tidak ditemukan.

Kedua fungsi ini digunakan untuk memeriksa keberadaan data di dalam file dan melakukan otentikasi pengguna berdasarkan informasi yang disimpan dalam file "users.table".

#### B. Autorisasi
- Untuk dapat mengakses database yang dia punya, permission dilakukan dengan command. Pembuatan tabel dan semua DML butuh untuk mengakses database terlebih dahulu.
- Yang bisa memberikan permission atas database untuk suatu user hanya root.
- User hanya bisa mengakses database di mana dia diberi permission untuk database tersebut.

```
void useDatabase(char *database)
{
    char data[300] = {0};
    sprintf(data, "%s\t%s\n", currentUser, database);//string data yang berisi currentUser dan nama database 
    int check = cekDataTersedia(data, "databases/init/permission.table", 1);//memeriksa data tersedia
    if (check)//jika data tersedia
    {
        strcpy(currentDatabase, database);//database akan disalin ke dalam variabel currentDatabase
        strcpy(pesan, "Gagal menggunakan database\n");
    }
    else
    {
        strcpy(pesan, "Database Not Found / You Not have Permission\n");
    }
    memset(data_exist, 0, sizeof(data_exist));
}

```
#### Fungsi `useDatabase`:

1. **Parameter:**
   - `char *database`: Nama database yang akan digunakan.

2. **Variabel-variabel:**
   - `data[300]`: Array karakter yang digunakan untuk menyimpan string yang berisi kombinasi `currentUser` (username saat ini) dan `database`.
   - `check`: Variabel yang akan menampung hasil dari pemanggilan fungsi `cekDataTersedia`.
   - `pesan`: Variabel untuk menyimpan pesan yang akan dikirim kepada pengguna.
   - `currentDatabase`: Variabel global yang akan menyimpan nama database yang sedang digunakan.

3. **Alur Fungsi:**
   - Fungsi ini menggabungkan `currentUser` (username saat ini) dengan `database` ke dalam string `data` menggunakan `sprintf`.
   - Memanggil fungsi `cekDataTersedia` dengan parameter `data` yang berisi informasi kombinasi username dan nama database yang akan diperiksa keberadaannya di file "databases/init/permission.table".
   - Jika data ditemukan di file "permission.table":
     - `currentDatabase` akan diisi dengan nama database yang diberikan sebagai parameter.
     - Pesan akan diisi dengan "Gagal menggunakan database" (mungkin ada kesalahan saat proses penyalinan, karena seharusnya ini adalah pesan sukses).
   - Jika data tidak ditemukan di file "permission.table":
     - Pesan akan diisi dengan "Database Not Found / You Not have Permission".

4. **Output:**
   - Variabel `pesan` akan berisi pesan yang memberitahu pengguna apakah pengguna berhasil atau gagal menggunakan database sesuai dengan informasi yang ada di file "permission.table".
   - Jika data ditemukan, variabel `currentDatabase` akan berisi nama database yang sedang digunakan.

Fungsi ini pada dasarnya memeriksa izin pengguna untuk menggunakan database yang ditentukan. Jika pengguna memiliki izin, fungsi akan mengubah variabel global `currentDatabase` ke nama database yang dipilih. Jika tidak ada izin atau nama database tidak ditemukan, pengguna akan diberi pesan kesalahan yang sesuai.

```
void grantDatabase(char *username, char *database)
{
    if (strcmp(currentUser, "root") == 0)//memeriksa apakah currentuser adl root
    {
        int check = 0;
        char data[2048] = {0};
        sprintf(data, "%s\t%s\n", username, database);//membuat data yang akan disimpan dalam file
        check = cekDataTersedia(data, "databases/init/permission.table", 1);//memeriksa kombinasi username dan database 
        if (check == 0)//jika data belum ada
        {
            //menambahkan data ke permission table
            FILE *fptr;
            fptr = fopen("databases/init/permission.table", "a+");
            if (fptr == NULL)
            {
                strcpy(pesan, "Error\n");

                return;
            }
            fprintf(fptr, "%s\t%s\n", username, database);
            memset(data_exist, 0, sizeof(data_exist));
            fclose(fptr);
            strcpy(pesan, "Syntax salah\n");
        }
        else//jika data sudah ada
        {
            strcpy(pesan, "User sudah memiliki akses ke database\n");
            memset(data_exist, 0, sizeof(data_exist));
            return;
        }
    }
    else
    {
        strcpy(pesan, "Bukan root\n");
    }
}

```
Fungsi `grantDatabase` memiliki tujuan untuk memberikan akses ke suatu database kepada pengguna tertentu. Berikut adalah penjelasan rinci dari fungsi ini:

##### Fungsi `grantDatabase`:

1. **Parameter:**
   - `char *username`: Username dari pengguna yang akan diberikan akses.
   - `char *database`: Nama database yang akan diberikan akses.

2. **Variabel-variabel:**
   - `check`: Variabel untuk menyimpan hasil dari pemanggilan fungsi `cekDataTersedia`.
   - `data[2048]`: Array karakter yang digunakan untuk menyimpan string yang berisi kombinasi `username` dan `database` yang akan ditambahkan ke file "databases/init/permission.table".
   - `pesan`: Variabel untuk menyimpan pesan yang akan diberikan kepada pengguna.
   - `FILE *fptr`: Pointer file untuk membuka dan menulis ke file "permission.table".

3. **Alur Fungsi:**
   - Fungsi ini pertama-tama memeriksa apakah `currentUser` adalah "root" (pengguna super administrator). Jika bukan, fungsi hanya memberikan pesan "Bukan root" dan tidak melanjutkan proses.
   - Jika `currentUser` adalah "root":
     - Fungsi membentuk string `data` yang berisi kombinasi `username` dan `database` yang akan ditambahkan ke file "permission.table".
     - Memanggil fungsi `cekDataTersedia` untuk memeriksa apakah data yang sama sudah ada di dalam file "permission.table".
     - Jika data belum ada:
       - Fungsi membuka file "permission.table" dalam mode append (`"a+"`), dan jika gagal membuka file, akan memberikan pesan "Error".
       - Menulis kombinasi `username` dan `database` ke dalam file "permission.table".
       - Mengosongkan `data_exist`.
       - Menutup file setelah penulisan selesai dan memberikan pesan "Syntax salah" (mungkin pesan seharusnya adalah "Database telah diberikan izin kepada pengguna").
     - Jika data sudah ada di file "permission.table":
       - Fungsi memberikan pesan "User sudah memiliki akses ke database".
     - Jika `currentUser` bukan "root", fungsi memberikan pesan "Bukan root".

4. **Output:**
   - Variabel `pesan` akan berisi pesan yang memberitahu pengguna apakah proses pemberian izin ke database kepada pengguna tertentu berhasil atau gagal, sesuai dengan hasil verifikasi yang dilakukan berdasarkan peran `currentUser`.
   - Fungsi ini berpotensi menambahkan baris baru ke file "permission.table" jika izin belum diberikan sebelumnya dan `currentUser` adalah "root".

Fungsi ini pada dasarnya adalah mekanisme untuk memberikan izin kepada pengguna tertentu untuk mengakses suatu database dengan syarat bahwa `currentUser` adalah "root".

### C.Data Definition Language
- Input penamaan database, tabel, dan kolom hanya angka dan huruf.
- Semua user bisa membuat database, otomatis user tersebut memiliki permission untuk database tersebut.
- Root dan user yang memiliki permission untuk suatu database untuk bisa membuat tabel untuk database tersebut, tentunya setelah mengakses database tersebut. Tipe data dari semua kolom adalah string atau integer. Jumlah kolom bebas.
- Bisa melakukan DROP database, table (setelah mengakses database), dan kolom. Jika sasaran drop ada maka di-drop, jika tidak ada maka biarkan.

```
void createDatabase(char *database)
{
    char fullpath[200] = {0};
    sprintf(fullpath, "databases/%s", database);//membuat path ke database
    int check = mkdir(fullpath, 0777);//membuat file di path

    if (!check)//direktori berhasil dibuat
    {
        FILE *fptr;
        fptr = fopen("databases/init/permission.table", "a+");// membuka file permission

        if (fptr == NULL)//error handling
        {
            strcpy(pesan, "File Gagal Dibuka\n");
            return;
        }
        if (strcmp(currentUser, "root") == 0)//jika current user adalah root
        {
            fprintf(fptr, "%s\t%s\n", currentUser, database);//Menulis entri baru ke dalam file permission.
            fclose(fptr);
        }
        else//jika current user bukan root
        {
            fprintf(fptr, "%s\t%s\nroot\t%s\n", currentUser, database, database);//Menulis entri baru ke dalam file permission. 
            fclose(fptr);
        }
        strcpy(pesan, "Terjadi Kesalahan\n");
    }
    else
    {
        strcpy(pesan, "Database Gagal Dibuat\n");
        return;
    }
}

```
#### Fungsi `createDatabase`:

1. **Parameter:**
   - `char *database`: Nama database yang akan dibuat.

2. **Variabel-variabel:**
   - `fullpath[200]`: Array karakter yang digunakan untuk menyimpan path lengkap ke direktori yang akan dibuat.
   - `check`: Variabel untuk menyimpan nilai kembalian dari fungsi `mkdir`.
   - `pesan`: Variabel untuk menyimpan pesan yang akan diberikan kepada pengguna.
   - `FILE *fptr`: Pointer file untuk membuka dan menulis ke file "permission.table".

3. **Alur Fungsi:**
   - Fungsi ini menggabungkan string "databases/" dengan `database` ke dalam string `fullpath` menggunakan `sprintf` untuk membentuk path lengkap ke direktori yang akan dibuat.
   - Menggunakan fungsi `mkdir` untuk membuat direktori baru dengan nama yang telah ditentukan.
   - Jika direktori berhasil dibuat (`mkdir` mengembalikan nilai 0):
     - Fungsi membuka file "permission.table" dalam mode append (`"a+"`).
     - Jika gagal membuka file, akan diberikan pesan "File Gagal Dibuka".
     - Jika `currentUser` adalah "root":
       - Fungsi menulis entri baru yang berisi kombinasi `currentUser` dan `database` ke dalam file "permission.table".
     - Jika `currentUser` bukan "root":
       - Fungsi menulis entri baru yang mencakup `currentUser`, `database`, dan entri default "root" ke dalam file "permission.table".
     - Menutup file setelah penulisan selesai dan memberikan pesan "Terjadi Kesalahan".
   - Jika direktori gagal dibuat (`mkdir` mengembalikan nilai selain 0):
     - Memberikan pesan "Database Gagal Dibuat".

4. **Output:**
   - Variabel `pesan` akan berisi pesan yang memberitahu pengguna apakah pembuatan database berhasil atau gagal.
   - Fungsi ini membuat direktori baru dengan nama database dan mencatat entri yang sesuai di dalam file "permission.table" terkait dengan izin akses terhadap database tersebut.

Fungsi ini bertanggung jawab untuk membuat direktori baru yang akan berfungsi sebagai database dan mencatat izin akses untuk pengguna (terutama "root") ke dalam file "permission.table". Jika pengguna yang memanggil fungsi bukan "root", maka secara otomatis pengguna "root" akan diberikan akses ke database yang baru dibuat bersama dengan pengguna yang meminta pembuatan database.

```
void createTable(char *table, char *column_name)
{
    FILE *fptr;
    char fullpath[500] = {0};
    
    // Menggabungkan path database dan nama tabel untuk membentuk path lengkap
    sprintf(fullpath, "databases/%s/%s", currentDatabase, table);
    
    // Membuka file dengan mode "a+" (append plus) untuk menambahkan ke file atau membuat jika belum ada
    fptr = fopen(fullpath, "a+");
    
    // Memeriksa apakah file berhasil dibuka
    if (fptr == NULL)
    {
        strcpy(pesan, "Error\n"); // Pesan kesalahan jika gagal membuka file
        return; // Menghentikan fungsi
    }
    
    // Menulis nama kolom ke dalam file
    fprintf(fptr, "%s\n", column_name);
    
    // Menutup file setelah selesai menulis
    fclose(fptr);
    
    // Memberikan pesan bahwa tabel berhasil dibuat
    strcpy(pesan, "Berhasil membuat table\n");
}

```

#### Fungsi `createTable`:

1. **Parameter:**
   - `char *table`: Nama tabel yang akan dibuat.
   - `char *column_name`: Nama kolom yang akan ditambahkan ke tabel.

2. **Variabel-variabel:**
   - `fptr`: Pointer file untuk membuka dan menulis ke file tabel.
   - `fullpath[500]`: Array karakter yang digunakan untuk menyimpan path lengkap ke file tabel.
   - `pesan`: Variabel untuk menyimpan pesan yang akan diberikan kepada pengguna.

3. **Alur Fungsi:**
   - Fungsi ini menggabungkan string "databases/" dengan `currentDatabase` (nama database saat ini) dan `table` untuk membentuk path lengkap ke file tabel menggunakan `sprintf`.
   - Membuka file dengan mode "a+" (append plus) menggunakan `fopen` untuk menambahkan ke file atau membuat file baru jika belum ada.
   - Memeriksa apakah file berhasil dibuka. Jika tidak (`fptr == NULL`):
     - Memberikan pesan kesalahan "Error" kepada pengguna.
     - Menghentikan eksekusi fungsi.
   - Jika file berhasil dibuka:
     - Menulis `column_name` (nama kolom) ke dalam file tabel menggunakan `fprintf`.
     - Menutup file setelah selesai menulis.
     - Memberikan pesan bahwa tabel berhasil dibuat kepada pengguna.

4. **Output:**
   - Variabel `pesan` akan berisi pesan yang memberitahu pengguna apakah pembuatan tabel berhasil atau jika terdapat kesalahan dalam proses pembuatan.

Fungsi ini bertanggung jawab untuk membuat file tabel di dalam database yang sedang digunakan. Prosesnya melibatkan pembuatan path lengkap ke file tabel, membuka file tersebut, menambahkan nama kolom, dan memberikan pesan kepada pengguna apakah pembuatan tabel berhasil atau tidak.

```
void dropDatabase(char *data, char *database)
{
    tester = 0; // Set nilai variabel global tester menjadi 0

    char fullpathCommand[2048] = {0}; // Deklarasi string untuk menyimpan perintah lengkap
    sprintf(fullpathCommand, "rm -r databases/%s", database); // Membuat perintah untuk menghapus database dengan menggunakan "rm -r" di sistem Unix/Linux

    system(fullpathCommand); // Melakukan eksekusi perintah menggunakan fungsi system() untuk menghapus database dari sistem

    readDatabase("init", "permission.table"); // Membaca isi tabel permission dari database "init" setelah penghapusan

    writeTable("databases/init/permission.table"); // Menulis kembali tabel permission ke file "permission.table" di database "init"

    strcpy(pesan, "Syntax salah\n"); // Mengatur pesan kesalahan ke "Syntax salah" (mungkin perlu penyesuaian, karena ini tampaknya bukan pesan kesalahan yang tepat untuk operasi drop database)
}

```
#### Fungsi `dropDatabase`:

1. **Parameter:**
   - `char *data`: Data yang digunakan dalam proses ini (tampaknya tidak digunakan dalam fungsi).
   - `char *database`: Nama database yang akan dihapus.

2. **Variabel-variabel:**
   - `tester`: Variabel global yang digunakan sebagai penanda, dalam konteks ini diatur nilainya menjadi 0.
   - `fullpathCommand[2048]`: Array karakter yang digunakan untuk menyimpan perintah lengkap untuk menghapus database.
   - `pesan`: Variabel untuk menyimpan pesan yang akan diberikan kepada pengguna.

3. **Alur Fungsi:**
   - Mengatur variabel global `tester` menjadi 0.
   - Membentuk string `fullpathCommand` yang berisi perintah untuk menghapus direktori database menggunakan perintah "rm -r" di sistem Unix/Linux dengan `sprintf`.
   - Melakukan eksekusi perintah menggunakan fungsi `system()` untuk menjalankan perintah penghapusan database dari sistem.
   - Memanggil fungsi `readDatabase("init", "permission.table")` untuk membaca isi tabel permission dari database "init" setelah penghapusan database dilakukan.
   - Menulis kembali tabel permission ke file "permission.table" di database "init" menggunakan fungsi `writeTable("databases/init/permission.table")`.
   - Mengatur pesan kesalahan ke "Syntax salah" (catatan: pesan ini tampaknya tidak relevan dengan operasi penghapusan database).

4. **Output:**
   - Variabel `pesan` akan berisi pesan yang memberitahu pengguna tentang hasil operasi. Namun, dalam implementasi saat ini, pesan yang ditetapkan adalah "Syntax salah", yang mungkin tidak menggambarkan dengan tepat hasil dari operasi penghapusan database.

Fungsi ini pada dasarnya menggunakan perintah sistem untuk menghapus sebuah database dan selanjutnya memperbarui informasi tentang izin-izin pengguna di dalam tabel permission. Meskipun demikian, pesan yang dihasilkan tampaknya belum sepenuhnya sesuai dengan aksi yang dilakukan, dan bisa diperlukan penyesuaian untuk memberikan informasi yang lebih akurat kepada pengguna tentang hasil operasi penghapusan database.

```
void dropTable(char *table) {
    tester = 0; // Inisialisasi variabel tester (mungkin variabel global) dengan nilai 0
    char fullpath[500] = {0}; // Membuat array untuk menyimpan path lengkap tabel
    // Menggunakan sprintf untuk membuat path lengkap tabel dengan format "databases/{currentDatabase}/{table}"
    sprintf(fullpath, "databases/%s/%s", currentDatabase, table);

    // Menghapus file (tabel) dengan path yang telah dibuat
    if (remove(fullpath) == 0) // Jika penghapusan berhasil (remove() mengembalikan nilai 0)
        strcpy(pesan, "Berhasil drop\n"); // Set pesan ke "Berhasil drop" jika penghapusan berhasil
    else
        strcpy(pesan, "Error\n"); // Set pesan ke "Error" jika terjadi kesalahan saat penghapusan
}

```

#### Fungsi `dropTable`:

1. **Parameter:**
   - `char *table`: Nama tabel yang akan dihapus.

2. **Variabel-variabel:**
   - `tester`: Variabel global yang digunakan sebagai penanda, dalam konteks ini diatur nilainya menjadi 0.
   - `fullpath[500]`: Array karakter yang digunakan untuk menyimpan path lengkap ke file tabel.
   - `pesan`: Variabel untuk menyimpan pesan yang akan diberikan kepada pengguna.

3. **Alur Fungsi:**
   - Mengatur variabel global `tester` menjadi 0.
   - Membentuk string `fullpath` yang berisi path lengkap ke file tabel dengan menggunakan `sprintf`, yang akan berformat "databases/{currentDatabase}/{table}".
   - Menghapus file (tabel) menggunakan fungsi `remove()` dengan path yang telah dibuat.
   - Memeriksa apakah penghapusan berhasil dengan mengecek nilai yang dikembalikan oleh `remove()`. Jika penghapusan berhasil (`remove()` mengembalikan nilai 0):
     - Mengatur pesan ke "Berhasil drop".
   - Jika terjadi kesalahan saat penghapusan (nilai yang dikembalikan oleh `remove()` bukan 0):
     - Mengatur pesan ke "Error".

4. **Output:**
   - Variabel `pesan` akan berisi pesan yang memberitahu pengguna tentang hasil operasi penghapusan tabel, apakah berhasil atau terdapat kesalahan.

Fungsi ini secara khusus menghapus file tabel dari direktori yang sesuai dengan nama database yang sedang digunakan. Jika penghapusan berhasil, pesan "Berhasil drop" akan diberikan kepada pengguna, dan jika terdapat kesalahan saat penghapusan, pesan "Error" akan diberikan.

```
void dropColumn(int position)
{
    char temp[200][2048] = {0};
    char hasil[200][2048] = {0};
    for (int i = 0; i < count_row; i++)
    {
        strcpy(temp[i], currData[i]);
    }

    for (int i = 0; i < count_row; i++)
    {

        char *token = strtok(temp[i], "\t");
        int count = 0;
        while (count < count_column)
        {

            if (count == position)
            {
                token = strtok(NULL, "\t");
                count++;
                continue;
                tester=1;
            }
            else
            {

                strcat(hasil[i], token);
                if (!strstr(hasil[i], "\n"))
                {
                    strcat(hasil[i], "\t");
                }
                token = strtok(NULL, "\t");
                count++;
            }
        }

        if (!strstr(hasil[i], "\n"))
        {   tester =1;
            strncpy(hasil[i], hasil[i], strlen(hasil[i]) - 1);
            strcat(hasil[i], "\n");
        }
    }
    for (int i = 0; i < count_row; i++)
    {
        strcpy(currData[i], hasil[i]);
    }
    for (int i = 0; i < 100; i++)
    {
        data_exist[i] = -2;
    }
}
```
#### Fungsi `dropColumn`:

1. **Parameter:**
   - `int position`: Posisi kolom yang akan dihapus.

2. **Variabel-variabel:**
   - `temp[200][2048]`: Array dua dimensi yang menyimpan salinan sementara dari struktur data sebelum modifikasi.
   - `hasil[200][2048]`: Array dua dimensi yang menyimpan hasil akhir setelah kolom dihapus.
   - `count_row`: Variabel global yang menyimpan jumlah baris data.
   - `count_column`: Variabel global yang menyimpan jumlah kolom data.
   - `currData`: Array dua dimensi yang menyimpan struktur data yang sedang diproses.
   - `tester`: Variabel global yang digunakan sebagai penanda.

3. **Alur Fungsi:**
   - Membuat salinan sementara dari struktur data yang ada (`currData`) ke dalam array `temp`.
   - Melakukan iterasi melalui setiap baris data (`count_row`):
     - Menggunakan fungsi `strtok` untuk membagi baris menjadi token-token terpisah berdasarkan delimiter `\t` (tab).
     - Iterasi di dalam baris untuk menemukan dan melewati kolom yang akan dihapus (`position`).
     - Menggabungkan token-token yang tidak termasuk kolom yang akan dihapus ke dalam array `hasil`.
     - Memperbarui struktur data `hasil` dengan menghilangkan tab atau newline yang tidak diperlukan.
   - Memperbarui struktur data awal `currData` dengan struktur data yang telah dimodifikasi dalam array `hasil`.
   - Menginisialisasi array `data_exist` dengan nilai -2 untuk mempersiapkan ulang indeks data yang ada.

4. **Output:**
   - Fungsi ini melakukan modifikasi langsung pada struktur data `currData` dengan menghapus kolom pada posisi yang ditentukan. Variabel `tester` digunakan sebagai penanda untuk perubahan yang dilakukan dalam proses tersebut.

Fungsi `dropColumn` bekerja dengan cara menghapus kolom pada struktur data yang diwakili dalam bentuk array dua dimensi dengan memanipulasi token dan menggabungkan kembali bagian-bagian yang sesuai untuk menghasilkan struktur data baru tanpa kolom yang dimaksud. Variabel `tester` kemungkinan digunakan untuk memberi tanda bahwa ada modifikasi yang terjadi pada struktur data dalam proses tersebut.

### 3. Data Manipulation Language
- INSERT
    <br>Hanya bisa insert satu row per satu command. Insert sesuai dengan jumlah dan urutan kolom.
- UPDATE
    <br> Hanya bisa update satu kolom per satu command.
- DELETE
    <br>Delete data yang ada di tabel.
- SELECT
- WHERE
    <br>Command UPDATE, SELECT, dan DELETE bisa dikombinasikan dengan WHERE. WHERE hanya untuk satu kondisi. Dan hanya ‘=’.

```
void insert(char *data, char *table) {
    FILE *fptr; // Deklarasi pointer untuk file
    char fullpath[200] = {0}; // Variabel untuk menyimpan path lengkap file
    sprintf(fullpath, "databases/%s/%s", currentDatabase, table); // Membentuk path lengkap file berdasarkan currentDatabase dan table

    fptr = fopen(fullpath, "a+"); // Membuka file untuk ditambahkan data ("a+") atau membuat baru jika tidak ada
    if (fptr == NULL) {
        strcpy(pesan, "Error\n"); // Jika gagal membuka file, mengatur pesan error

        return; // Menghentikan fungsi
    }

    fprintf(fptr, "%s\n", data); // Menulis data ke dalam file dengan format tertentu
    fclose(fptr); // Menutup file setelah selesai menulis

    strcpy(pesan, "Berhasil insert data\n"); // Jika berhasil, mengatur pesan berhasil
}
```
#### Fungsi `insert`:

1. **Parameter:**
   - `char *data`: Data yang akan dimasukkan ke dalam tabel.
   - `char *table`: Nama tabel tempat data akan dimasukkan.

2. **Variabel-variabel:**
   - `fptr`: Pointer file untuk membuka dan menulis ke file.
   - `fullpath[200]`: Array karakter yang digunakan untuk menyimpan path lengkap ke file tabel.
   - `pesan`: Variabel untuk menyimpan pesan yang akan diberikan kepada pengguna.

3. **Alur Fungsi:**
   - Fungsi ini membentuk path lengkap ke file tabel menggunakan `sprintf` dengan format "databases/{currentDatabase}/{table}".
   - Membuka file dalam mode "a+" (append plus) menggunakan `fopen`, yang akan menambahkan data ke file jika sudah ada atau membuat file baru jika belum ada.
   - Memeriksa apakah file berhasil dibuka. Jika tidak (`fptr == NULL`):
     - Mengatur pesan error "Error".
     - Menghentikan eksekusi fungsi.
   - Jika file berhasil dibuka:
     - Menulis data ke dalam file menggunakan `fprintf` dengan format tertentu.
     - Menutup file setelah selesai menulis.
     - Mengatur pesan berhasil "Berhasil insert data".

4. **Output:**
   - Variabel `pesan` akan berisi pesan yang memberitahu pengguna apakah proses penambahan data ke tabel berhasil atau tidak.

Fungsi ini bertanggung jawab untuk menambahkan data ke dalam file tabel yang ditentukan. Jika file berhasil dibuka dan data berhasil ditambahkan, pesan "Berhasil insert data" akan diberikan kepada pengguna. Namun, jika terjadi kesalahan saat membuka file atau menambahkan data, pesan error akan diberikan kepada pengguna.

```
void updateData(int mode, int position, char *data_column, char *search_column, char *search_data)
{
    if (mode == 1)
    {
        // Mode 1: Memperbarui data pada posisi tertentu di setiap baris
        char tmp[200][2048] = {0}; // Array untuk menyimpan salinan sementara dari data
        char hasil[200][2048] = {0}; // Array untuk menyimpan hasil pembaruan data

        // Menyalin data ke array sementara (tmp)
        for (int i = 0; i < count_row; i++)
        {
            strncpy(tmp[i], currData[i], strlen(currData[i]) - 1); // Salin setiap baris ke array sementara
        }

        // Memproses pembaruan data untuk setiap baris
        for (int i = 0; i < count_row; i++)
        {
            char *token = strtok(tmp[i], "\t"); // Memecah baris menjadi token berdasarkan tab
            int j = 0;

            while (token != NULL)
            {
                if (j == position)
                {
                    // Jika posisi sesuai dengan kolom yang ingin diperbarui, tambahkan data_column
                    strcat(hasil[i], data_column);
                    strcat(hasil[i], "\t");
                }
                else
                {
                    // Jika tidak, tambahkan token yang ada dari data yang sudah ada
                    strcat(hasil[i], token);
                    strcat(hasil[i], "\t");
                }
                token = strtok(NULL, "\t"); // Pindah ke token berikutnya
                j++;
            }

            // Mengatur karakter terminasi string
            hasil[i][strlen(hasil[i]) - 1] = '\0';
            if (!strstr(hasil[i], "\n"))
            {
                strcat(hasil[i], "\n");
            }
        }

        // Menyalin hasil pembaruan data kembali ke currData
        for (int i = 0; i < count_row; i++)
        {
            strcpy(currData[i], hasil[i]);
        }
        strcpy(pesan, "berhasil \n"); // Pesan berhasil
    }
    else if (mode == 2)
    {
        // Mode 2: Memperbarui data berdasarkan pencocokan dengan data tertentu
        char tmp[200][2048] = {0}; // Array untuk menyimpan salinan sementara dari data
        char hasil[200][2048] = {0}; // Array untuk menyimpan hasil pembaruan data

        // Menyalin data ke array sementara (tmp)
        for (int i = 0; i < count_row; i++)
        {
            strncpy(tmp[i], currData[i], strlen(currData[i])); // Salin setiap baris ke array sementara
        }

        int j = 0;
        // Memproses pembaruan data berdasarkan ketersediaan data tertentu
        for (int i = 0; i < count_row; i++)
        {
            if (i == data_exist[j])
            {
                char *token = strtok(tmp[i], "\t"); // Memecah baris menjadi token berdasarkan tab
                int k = 0;

                while (token != NULL)
                {
                    if (k == position)
                    {
                        // Jika posisi sesuai dengan kolom yang ingin diperbarui, tambahkan data_column
                        strcat(hasil[i], data_column);
                        strcat(hasil[i], "\t");
                    }
                    else
                    {
                        // Jika tidak, tambahkan token yang ada dari data yang sudah ada
                        strcat(hasil[i], token);
                        strcat(hasil[i], "\t");
                    }
                    token = strtok(NULL, "\t"); // Pindah ke token berikutnya
                    k++;
                }

                // Mengatur karakter terminasi string
                hasil[i][strlen(hasil[i]) - 1] = '\0';
                j++;
            }
            else
            {
                // Jika tidak sesuai dengan data yang dicari, tambahkan data yang ada ke hasil
                strcat(hasil[i], tmp[i]);
            }

            // Menambahkan karakter terminasi dan baris baru
            if (!strstr(hasil[i], "\n"))
            {
                strcat(hasil[i], "\n");
            }
        }

        // Menyalin hasil pembaruan data kembali ke currData
        for (int i = 0; i < count_row; i++)
        {
            strcpy(currData[i], hasil[i]);
        }
        strcpy(pesan, "berhasil \n"); // Pesan berhasil
    }
}
```
Fungsi `updateData` dirancang untuk memperbarui data di dalam struktur data yang direpresentasikan dalam bentuk array dua dimensi (`currData`). 

#### Fungsi `updateData`:

1. **Parameter:**
   - `int mode`: Mode operasi, yang dapat bernilai 1 atau 2. Mode 1 untuk memperbarui data pada posisi tertentu di setiap baris, sedangkan mode 2 untuk memperbarui data berdasarkan pencocokan dengan data tertentu.
   - `int position`: Posisi kolom yang akan diperbarui.
   - `char *data_column`: Data baru yang akan dimasukkan ke dalam kolom yang ditentukan.
   - `char *search_column`: Kolom yang akan dicocokkan untuk pembaruan dalam mode 2.
   - `char *search_data`: Data yang akan dicocokkan dalam mode 2.

2. **Variabel-variabel:**
   - `tmp[200][2048]`: Array dua dimensi untuk menyimpan salinan sementara dari struktur data sebelum pembaruan.
   - `hasil[200][2048]`: Array dua dimensi untuk menyimpan hasil akhir dari proses pembaruan data.
   - `count_row`: Variabel global yang menyimpan jumlah baris data.
   - `currData`: Array dua dimensi yang menyimpan struktur data yang sedang diproses.
   - `data_exist`: Array yang menyimpan indeks baris data tertentu yang memenuhi kriteria pencarian dalam mode 2.
   - `pesan`: Variabel untuk menyimpan pesan yang akan diberikan kepada pengguna.

3. **Alur Fungsi:**
   - Fungsi ini memiliki dua mode operasi tergantung pada nilai parameter `mode`.
     - **Mode 1:** Memperbarui data pada posisi tertentu di setiap baris.
     - **Mode 2:** Memperbarui data berdasarkan pencocokan dengan data tertentu.

4. **Output:**
   - Variabel `currData` yang merepresentasikan struktur data yang ada akan diperbarui sesuai dengan perubahan yang dilakukan dalam fungsi ini.
   - Variabel `pesan` akan berisi pesan yang memberitahu pengguna apakah proses pembaruan data berhasil atau tidak.

```
void deleteData(int mode, int position, char *fullpath)
{
  
    
    // Mode 1: Mode untuk menulis ulang file dengan isi dari variabel column
    if (mode == 1)
    { 
        FILE *fptr;
        fptr = fopen(fullpath, "w+"); // Membuka file untuk ditulis ulang
        if (fptr == NULL)
        {
            strcpy(pesan, "Error\n"); // Mengatur pesan error jika gagal membuka file
            return;
        }
        fprintf(fptr, "%s", column); // Menulis isi dari variabel column ke dalam file
        strcpy(pesan, "berhasil \n"); // Mengatur pesan berhasil
        fclose(fptr); // Menutup file setelah selesai
    }
    // Mode 2: Mode untuk menghapus data pada posisi yang ditentukan
    else if (mode == 2)
    {

        FILE *fptr;
        int i = -1, j = 0;
        fptr = fopen(fullpath, "w+"); // Membuka file untuk ditulis ulang
        if (fptr == NULL)
        {
            strcpy(pesan, "Error\n"); // Mengatur pesan error jika gagal membuka file
            return;
        }
        // Menulis ulang isi file dengan melakukan manipulasi data sesuai mode
        for (int i = -1; i < count_row; i++)
        {
            if (i == -1)
            {
                fprintf(fptr, "%s", column); // Menulis data dari variabel column
            }
            else
            {
                // Mengatur data untuk ditulis ke file berdasarkan kondisi tertentu
                if (i == data_exist[j])
                {
                    j++;
                    continue; // Melewati data yang akan dihapus
                }
                fprintf(fptr, "%s", currData[i]); // Menulis data ke file
            }
        }
        strcpy(pesan, "berhasil \n"); // Mengatur pesan berhasil
        fclose(fptr); // Menutup file setelah selesai
    }
}
```
Fungsi `deleteData` bertujuan untuk melakukan operasi penghapusan data dalam file yang teridentifikasi oleh `fullpath`.
### Fungsi `deleteData`:

1. **Parameter:**
   - `int mode`: Mode operasi yang menentukan jenis operasi penghapusan yang akan dilakukan. Mode 1 untuk menulis ulang file dengan isi dari variabel `column`, sedangkan Mode 2 untuk menghapus data pada posisi yang ditentukan.
   - `int position`: Posisi yang akan dihapus dalam operasi Mode 2.
   - `char *fullpath`: Path lengkap file yang akan dioperasikan.

2. **Variabel-variabel:**
   - `fptr`: Pointer file untuk membuka dan menulis ke file.
   - `count_row`: Variabel global yang menyimpan jumlah baris data.
   - `currData`: Array dua dimensi yang menyimpan struktur data yang sedang diproses.
   - `data_exist`: Array yang menyimpan indeks baris data tertentu yang memenuhi kriteria pencarian.

3. **Alur Fungsi:**
   - Fungsi ini memiliki dua mode operasi:
     - **Mode 1:**
       - Membuka file untuk ditulis ulang dengan mode "w+" (menulis ulang file).
       - Jika file gagal dibuka, akan diatur pesan error.
       - Menulis isi dari variabel `column` ke dalam file.
       - Jika operasi berhasil, pesan "berhasil" diatur.
     - **Mode 2:**
       - Membuka file untuk ditulis ulang dengan mode "w+" (menulis ulang file).
       - Jika file gagal dibuka, akan diatur pesan error.
       - Menulis ulang isi file dengan melakukan manipulasi data berdasarkan kondisi tertentu:
         - Jika iterasi adalah -1, menulis data dari variabel `column`.
         - Jika terdapat pencocokan dengan `data_exist`, melewati data yang akan dihapus.
         - Menulis data yang tidak dihapus ke dalam file.
       - Jika operasi berhasil, pesan "berhasil" diatur.

4. **Output:**
   - Fungsi ini akan menghasilkan pesan yang memberitahu pengguna apakah operasi penghapusan berhasil atau tidak.

```
void selectCommand(int mode)
{
    char output[2048] = {0};
    int j = 0;
    int l = 0;
    char tmp[200][2048] = {0};
    char tmpcolumn[2048] = {0};
    char hasilcolumn[2048] = {0};
    char hasil[200][2048] = {0};

    //menyalin data dari currData ke tmp
    for (int i = 0; i < count_row; i++)
    {
        strcpy(tmp[i], currData[i]);
    }

    for (int i = -1; i < count_row; i++)
    {
        if (i == -1)
        {
            if (mode == 3 || mode == 4)
            {
                strcpy(tmpcolumn, column);
                char *token = strtok(tmpcolumn, "\t");
                int j = 0;
                int k = 0;
                while (token != NULL)
                {
                    if (j != position_column[k])
                    {
                        j++;
                        token = strtok(NULL, "\t");
                        continue;
                    }
                    else
                    {
                        strcat(hasilcolumn, token);
                        if (!strstr(hasilcolumn, "\n"))
                        {
                            strcat(hasilcolumn, "\t");
                        }
                        j++;
                        k++;
                        token = strtok(NULL, "\t");
                    }
                }
                if (!strstr(hasilcolumn, "\n"))
                {
                    strcat(hasilcolumn, "\n");
                }

                strcat(output, hasilcolumn);
            }
            else
            {
                strcat(output, column);
            }
        }
        else
        {
            if (mode == 1)
            {
                strcat(output, currData[i]);
            }
            if (mode == 2)
            {
                if (i == data_exist[j])
                {
                    strcat(output, currData[i]);
                    j++;
                }
                else
                {
                    continue;
                }
            }
            if (mode == 3)
            {
                char *token = strtok(tmp[i], "\t");
                int j = 0;
                int k = 0;
                while (token != NULL)
                {
                    if (j != position_column[k])
                    {
                        j++;
                        token = strtok(NULL, "\t");
                        continue;
                    }
                    else
                    {
                        strcat(hasil[i], token);
                        if (!strstr(hasil[i], "\n"))
                        {
                            strcat(hasil[i], "\t");
                        }
                        j++;
                        k++;
                        token = strtok(NULL, "\t");
                    }
                }
                if (!strstr(hasil[i], "\n"))
                {
                    strcat(hasil[i], "\n");
                }
                strcat(output, hasil[i]);
            }
            if (mode == 4)
            {
                char *token = strtok(tmp[i], "\t");
                int j = 0;
                int k = 0;

                while (token != NULL)
                {
                    if (j != position_column[k])
                    {
                        j++;
                        token = strtok(NULL, "\t");
                        continue;
                    }
                    else
                    {
                        strcat(hasil[i], token);
                        if (!strstr(hasil[i], "\n"))
                        {
                            strcat(hasil[i], "\t");
                        }
                        j++;
                        k++;
                        token = strtok(NULL, "\t");
                    }
                }
                if (!strstr(hasil[i], "\n"))
                {
                    strcat(hasil[i], "\n");
                }
                if (i == data_exist[l])
                {
                    strcat(output, hasil[i]);
                    l++;
                }
                else
                {
                    continue;
                }
            }
        }
    }
    strcpy(pesan, output);
}
```
Fungsi `selectCommand` dirancang untuk memilih data dari struktur data yang tersedia sesuai dengan mode yang ditentukan.

### Fungsi `selectCommand`:

1. **Parameter:**
   - `int mode`: Mode operasi yang menentukan cara pemilihan data.
   
2. **Variabel-variabel:**
   - `output[2048]`: Variabel yang akan menyimpan hasil seleksi data.
   - `tmp[200][2048]`: Array untuk menyimpan salinan sementara dari struktur data sebelum proses seleksi.
   - `tmpcolumn[2048]`, `hasilcolumn[2048]`: Variabel untuk menyimpan hasil seleksi kolom tertentu.
   - `hasil[200][2048]`: Array untuk menyimpan hasil akhir dari proses seleksi.
   - `count_row`: Variabel global yang menyimpan jumlah baris data.
   - `currData`: Array dua dimensi yang menyimpan struktur data yang sedang diproses.
   - `column`: Variabel yang menyimpan informasi kolom data.
   - `position_column`: Array yang menyimpan posisi kolom yang akan dipilih dalam mode tertentu.
   - `data_exist`: Array yang menyimpan indeks baris data tertentu yang memenuhi kriteria pencarian.
   - `pesan`: Variabel untuk menyimpan pesan yang akan diberikan kepada pengguna.

3. **Alur Fungsi:**
   - Fungsi ini melakukan proses pemilihan data berdasarkan mode yang ditentukan.
   - Untuk setiap mode:
     - **Mode 1:** Mengambil seluruh data yang ada.
     - **Mode 2:** Mengambil data yang memenuhi kriteria dari `data_exist`.
     - **Mode 3:** Mengambil kolom tertentu dari setiap baris data.
     - **Mode 4:** Mengambil kolom tertentu dari setiap baris data yang memenuhi kriteria dari `data_exist`.
   - Hasil seleksi data disimpan dalam variabel `output`.

4. **Output:**
   - Fungsi ini akan menghasilkan output dalam bentuk string yang memuat data yang telah dipilih sesuai dengan mode yang diminta oleh pengguna.

### E. Logging
- Setiap command yang dipakai harus dilakukan logging ke suatu file dengan format. Jika yang eksekusi root, maka username root.

```
void loging(char *input)
{

    FILE *f = fopen("database.log", "a+");
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    fprintf(f, "%04d-%02d-%02d %02d:%02d:%02d:%s:%s\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, currentUser, input);
    fclose(f);
}
```


#### Fungsi `loging`:

1. **Parameter:**
   - `char *input`: Merupakan string yang berisi informasi atau peristiwa yang akan dicatat dalam file log.

2. **Variabel-variabel:**
   - `FILE *f`: Pointer file yang digunakan untuk membuka dan menulis ke file log (`database.log`).
   - `time_t t`: Variabel waktu untuk menyimpan waktu saat ini.
   - `struct tm tm`: Struktur data waktu yang akan digunakan untuk menyimpan informasi tanggal dan waktu.
   - `currentUser`: Variabel global yang menyimpan informasi pengguna yang sedang aktif.
  
3. **Alur Fungsi:**
   - Fungsi membuka file `database.log` untuk ditambahkan data baru (mode "a+" yang artinya menambahkan data ke file jika file tersebut sudah ada).
   - Menggunakan fungsi `time()` untuk mendapatkan waktu saat ini dalam bentuk timestamp.
   - Mengonversi timestamp menjadi struktur data waktu lokal dengan bantuan fungsi `localtime()`.
   - Menggunakan `fprintf()` untuk menulis informasi ke dalam file log. Format penulisan mencakup:
     - Tanggal dan waktu (dalam format tahun-bulan-tanggal jam:menit:detik).
     - Informasi tentang pengguna yang melakukan aktivitas (`currentUser`).
     - Detail aktivitas yang terjadi (dalam parameter `input`).
   - Setelah selesai menulis, file log ditutup menggunakan fungsi `fclose()`.

4. **Output:**
   - Fungsi ini mencatat aktivitas atau peristiwa yang terjadi dalam sistem ke dalam file log `database.log` dalam format yang ditentukan. Hal ini membantu dalam memantau dan melacak aktivitas yang dilakukan oleh pengguna dalam sistem.

## Output
### Menjalankan server secara daemon 
![Imgur](https://i.imgur.com/qN0lpTo.png)

### Menjalankan client
![Imgur](https://i.imgur.com/9W6unii.png)

### Output Autentikasi, Autorisasi, dan DDL
Note: database berhasil di drop
![Imgur](https://i.imgur.com/me6DeSP.png)

### Output Autentikasi dan Autorisasi User
Note: database berhasil di drop
![Imgur](https://i.imgur.com/me6DeSP.png)

### Output DML
![Imgur](https://i.imgur.com/qnGmxNQ.png)

### Output Logging
![Imgur](https://i.imgur.com/xipHQCO.png)


## Revisi 
Tidak ada

## Kendala
Kesulitan membuat dump 
